const fs = require('fs')

const [version] = process.argv.slice(2)

const changelogFile = './CHANGELOG.md'

const contents = fs.readFileSync(changelogFile).toString()

const prevVersion = contents.match(/<!-- COMPARE LINK SECTION -->\r\n\[(.*?)\]/)[1]

let newContents = contents
  .replace(
    /<!-- COMPARE LINK SECTION -->\r\n/,
    `<!-- COMPARE LINK SECTION -->\r\n[${version}]: https://gitlab.com/dnd-5e/foundry-vtt/foundry-file-utils/-/compare/${prevVersion}...${version}\r\n`
  )
  .replace(
    /<!-- VERSION SECTION -->/,
    `<!-- VERSION SECTION -->\r\n## [${version}] - ${
      new Date().toLocaleString().split(' ')[0]
    }\r\n\r\n### Added\r\n\r\n### Changed\r\n\r\n### Fixed\r\n`
  )

fs.writeFileSync(changelogFile, newContents)
