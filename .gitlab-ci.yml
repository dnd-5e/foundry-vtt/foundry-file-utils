# Cache modules in between jobs
# https://docs.gitlab.com/ee/ci/caching/#cache-nodejs-dependencies
cache:
  key: $CI_COMMIT_REF_SLUG
  paths:
    - .npm/

stages:
  - build
  - create_tag
  - deploy

workflow:
  # Run this workflow when a commit title matches semver
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - if: '$CI_COMMIT_TITLE =~ /^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/'

variables:
  ZIP_FILE_NAME: 'foundry-file-utils-$CI_COMMIT_TITLE.zip'
  RELEASE_DOWNLOAD: 'https://gitlab.com/$CI_PROJECT_PATH/-/archive/$CI_COMMIT_TITLE/foundry-file-utils-$CI_COMMIT_TITLE.zip'

build:
  image: node:latest
  stage: build
  script:
    # Extract version from module.json
    - PACKAGE_VERSION=$(node -p "require('./src/module.json').version")
    - PACKAGE_DOWNLOAD=$(node -p "require('./src/module.json').download")

    # Validate that the "tag" being released matches the package version.
    - |
      if [[ ! $CI_COMMIT_TITLE == $PACKAGE_VERSION ]]; then
        echo "The module.json version does not match commit title."
        echo "module.json: $PACKAGE_VERSION"
        echo "commit title: $CI_COMMIT_TITLE"
        exit 1
      fi

    # Validate that the package download url matches the release asset that will be created.
    - |
      if [[ ! $RELEASE_DOWNLOAD == $PACKAGE_DOWNLOAD ]]; then
        echo "The module.json download url does not match the created release asset url."
        echo "module.json: $PACKAGE_DOWNLOAD"
        echo "release asset url: $RELEASE_DOWNLOAD"
        echo "Please fix this and push the tag again."
        exit 1
      fi

    # Use cached npm
    - npm ci --cache .npm --prefer-offline

    # Run build script
    - npm run build

    # install the zip package
    - apt-get update
    - apt-get install zip

    # zip module files
    - cd dist
    - zip ../$ZIP_FILE_NAME -r *
  artifacts:
    # include the module json and created zip file as artifacts for release, and lib folder for npm publish
    paths:
      - $ZIP_FILE_NAME
      - src/module.json
      - lib
    expire_in: never

# Create actual commit tag for release
tag:
  stage: create_tag
  image: curlimages/curl:latest
  script:
    - echo 'creating tag'
    - |
      curl --request POST --header "PRIVATE-TOKEN: $GITLAB_API_TOKEN" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/tags?tag_name=$CI_COMMIT_TITLE&ref=$CI_COMMIT_REF_NAME"

# Attach build artifacts as release assets
# https://docs.gitlab.com/ee/ci/yaml/index.html#complete-example-for-release
release:
  stage: deploy
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo 'running release_job'
  release:
    tag_name: '$CI_COMMIT_TITLE'
    ref: '$CI_COMMIT_TITLE'
    description: '**Installation:** To manually install this release, please use the following manifest URL: https://gitlab.com/$CI_PROJECT_PATH/-/releases/$CI_COMMIT_TITLE/downloads/src/module.json'
    assets:
      links:
        # links the release to the module manifest from this CI workflow
        - name: 'module.json'
          url: https://gitlab.com/$CI_PROJECT_PATH/-/jobs/artifacts/$CI_COMMIT_TITLE/raw/src/module.json?job=build
          filepath: '/src/module.json'
        # links the release to the zip file from this CI workflow
        - name: '$ZIP_FILE_NAME'
          url: https://gitlab.com/$CI_PROJECT_PATH/-/jobs/artifacts/$CI_COMMIT_TITLE/raw/$ZIP_FILE_NAME?job=build
          filepath: '/$ZIP_FILE_NAME'

# Publish NPM package
npm:
  stage: deploy
  image: 'node:14-alpine'
  variables:
    NPM_TOKEN: $NPM_TOKEN
  script:
    - echo 'running npm_publish'
    # create .npmrc file for authtentication
    - echo "//registry.npmjs.org/:_authToken=${NPM_TOKEN}" >> .npmrc
    - npm publish --access public
