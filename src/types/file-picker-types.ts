import { DataSource } from '..'
import { isFilePickerType, isObject } from '../utils'
import { isString } from '../utils/is-string'

interface FileMeta {
  path: string
  activeSource: DataSource
  bucket: string | null
}

// Convert default objects to json
const typeFunc = (val: string) => {
  return !isString(val) && isObject(val) ? JSON.stringify(val) : val // have to be stringified since foundry puts the value in a text input.
}

const TYPE_PREFIX = 'FileUtilityType'

// Type constructor factory for registering settings
const FilePickerType = (type?: FilePicker.Type) => {
  // bypass factory if someone accidentally uses the factory function itself as the picker type
  if (type && !isFilePickerType(type)) {
    return typeFunc(type)
  }

  // Emulate a "constructor" for the requested type
  const func = (val: string) => typeFunc(val)
  func.prototype = Object.getPrototypeOf(Object)
  Object.defineProperty(func, 'name', {
    value: `${TYPE_PREFIX}_${type}`,
  })
  return func as unknown as ConstructorOf<FileMeta> // We cheat a little to avoid inconsequential (bold assumption) type errors
}

// Fallback if someone accidentally uses the factory function itself as the picker type
FilePickerType.prototype = Object.getPrototypeOf(Object)
Object.defineProperty(FilePickerType, 'name', {
  value: `${TYPE_PREFIX}_`,
})

export { TYPE_PREFIX, FilePickerType as FilePicker }
export type { FileMeta }
