import { isString } from '../utils/is-string'
import { DataSource } from './data-source'
import { UploadOptions } from './upload-options'

const isS3ExistsError = (err: string) => (err ? err.toLowerCase().startsWith('the s3 key') : false)
const isFileExistsError = (err: string) => (err ? err.toLowerCase().startsWith('eexist') : false)

const _createDirectory = async <T extends DataSource>(
  ...[source, path, options]: [source: T, path: string, options?: UploadOptions<T>]
) => {
  try {
    await FilePicker.createDirectory(source as any, path, options)
  } catch (err: unknown) {
    const e = isString(err) ? (err as string) : (err as Error).message
    if (!isFileExistsError(e) && !isS3ExistsError(e)) {
      throw e
    }
  }
}

/** Conditional function params, if source is s3 then 'options' is required
 *  @example createDirectory('data', 'path/to/dir')
 *  @example createDirectory('s3', 'path/to/dir', { bucket: 'bucket-name'} )
 */
const createDirectory = async <T extends DataSource>(
  ...[source, path, options]: T extends 's3'
    ? [source: T, path: string, options: UploadOptions<T>]
    : [source: T, path: string, options?: UploadOptions<T>]
): Promise<boolean> => {
  try {
    if (source === 'forgevtt') {
      await _createDirectory(source, path, options)
    } else {
      const segments = path.split('/')

      let current = ''
      for (const dir of segments) {
        if (dir) {
          current += `${current ? '/' : ''}${dir}`
          await _createDirectory(source, current, options)
        }
      }
    }
  } catch (err: unknown) {
    console.error(err)
    return false
  }
  return true
}

export { createDirectory }
