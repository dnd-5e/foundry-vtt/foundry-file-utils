import { DataSource } from './data-source'
import { downloadFile } from './download-file'
import { uploadFile } from './upload-file'
import { UploadOptions } from './upload-options'

/** Conditional function params, if source is s3 then 'options' is required
 *  @example uploadFileFromUrl()('data', 'path/to/dir')('https://example.com/myfile.jpg')
 *  @example uploadFileFromUrl()('s3', 'path/to/dir', { bucket: 'bucket-name'} )('https://example.com/myfile.jpg', '0001.jpg')
 *  @example uploadFileFromUrl(transformer)('data', 'path/to/dir')('https://example.com/myfile.jpg', '0001.jpg')
 */
const uploadFileFromUrl =
  (transform?: (input: File) => File | Promise<File>) =>
  <T extends DataSource>(
    ...[source, path, options]: T extends 's3'
      ? [source: T, path: string, options: UploadOptions<T>]
      : [source: T, path: string, options?: UploadOptions<T>]
  ) =>
  async (url: string, filename?: string) => {
    const file = await downloadFile(url, filename)

    if (!file) {
      throw new Error(`unable to download file from ${url}`)
    }

    let modified = file
    if (transform) {
      modified = await transform(file)
    }

    return await uploadFile(source as any, path, modified, options)
  }

export { uploadFileFromUrl }
