type ForgeDataSource = 'forge-bazaar' | 'forgevtt'
type DataSource = FilePicker.DataSource | ForgeDataSource

const DATA_SOURCE_MAP: { [x: string]: string } = {
  data: 'data',
  public: 'core',
  s3: 'S3',
  'forge-bazaar': 'Bazaar',
  forgevtt: 'Assets',
}

export { DataSource, DATA_SOURCE_MAP }
