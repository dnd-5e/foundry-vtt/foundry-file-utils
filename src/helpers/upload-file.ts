import { DataSource } from './data-source'
import { UploadOptions } from './upload-options'

/** Conditional function params, if source is s3 then 'options' is required
 *  @example uploadFile('data', 'path/to/dir', file:File)
 *  @example uploadFile('s3', 'path/to/dir', file, { bucket: 'bucket-name'} )
 */
const uploadFile = async <T extends DataSource>(
  ...[source, path, file, options]: T extends 's3'
    ? [source: T, path: string, file: File, options: UploadOptions<T>]
    : [source: T, path: string, file: File, options?: UploadOptions<T>]
) => {
  return await FilePicker.upload(source as any, path, file, options)
}

export { uploadFile }
