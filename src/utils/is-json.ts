const isJson = (str: unknown) => {
  try {
    JSON.parse(str as string)
    return true
  } catch (err) {
    return false
  }
}

export { isJson }
